program ColorPicker;

uses
  System.StartUpCopy,
  FMX.Forms,
  cp.forms.main in 'cp.forms.main.pas' {frmColorPicker},
  cp.forms.overlay in 'cp.forms.overlay.pas' {frmOverlay},
  cp.utils in 'cp.utils.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmColorPicker, frmColorPicker);
  Application.Run;
end.
