unit cp.forms.overlay;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs;

type
  TfrmOverlay = class(TForm)
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    FDoClick: TProc;
  public
    property DoClick : TProc read FDoClick write FDoClick;
    class procedure ShowOverlay(OnClick : TProc);
    class procedure HideOverlay;
  end;

implementation

uses FMX.Platform;

{$R *.fmx}

var
  frm : TfrmOverlay;

{ TfrmOverlay }

procedure TfrmOverlay.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if (Key = vkReturn) or (Key = vkSpace) then
    DoClick;
end;

procedure TfrmOverlay.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  DoClick;
end;

class procedure TfrmOverlay.HideOverlay;
begin
  FreeAndNil(frm);
end;

class procedure TfrmOverlay.ShowOverlay(OnClick : TProc);
var
  r : TRect;
begin
  r := (TPlatformServices.Current.GetPlatformService(IFMXMultiDisplayService) as IFMXMultiDisplayService).DesktopRect;
  frm := TfrmOverlay.Create(Application);
  frm.Left := r.Left;
  frm.Top := r.Top;
  frm.Width := r.Width;
  frm.Height := r.Height;
  frm.DoClick := OnClick;
  frm.Show;
end;

end.
