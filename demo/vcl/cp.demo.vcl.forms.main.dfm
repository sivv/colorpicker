object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'VCL ColorPicker Demo'
  ClientHeight = 266
  ClientWidth = 381
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object shpColor: TShape
    Left = 56
    Top = 48
    Width = 153
    Height = 169
    Shape = stRoundRect
    OnMouseUp = shpColorMouseUp
  end
  object btnChangeColor: TButton
    Left = 224
    Top = 104
    Width = 105
    Height = 57
    Caption = 'Change Color'
    TabOrder = 0
    OnClick = PickColor
  end
end
