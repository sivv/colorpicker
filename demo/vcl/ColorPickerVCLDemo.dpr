program ColorPickerVCLDemo;

uses
  Vcl.Forms,
  cp.demo.vcl.forms.main in 'cp.demo.vcl.forms.main.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
