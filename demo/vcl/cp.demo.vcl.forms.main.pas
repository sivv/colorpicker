unit cp.demo.vcl.forms.main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TfrmMain = class(TForm)
    shpColor: TShape;
    btnChangeColor: TButton;
    procedure PickColor(Sender: TObject);
    procedure shpColorMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    procedure WMCopyData(var Msg : TWMCopyData); message WM_COPYDATA;
  public
  end;

var
  frmMain: TfrmMain;

implementation

uses Winapi.ShellAPI, System.UIConsts, System.UITypes, cp.utils;

{$R *.dfm}

procedure TfrmMain.PickColor(Sender: TObject);
begin
  if not btnChangeColor.Enabled then
    exit;
  btnChangeColor.Enabled := False;
  Caption := Cardinal(Self.Handle).ToString;
  ShellExecute(0, 'open', 'ColorPicker.exe', PChar(Cardinal(Self.Handle).ToString+' '+TColors.AlphaColorToString(TColors.ColorToAlphaColor(shpColor.Brush.Color))), '..\..\..\..\Win32\Debug\', SW_SHOWNORMAL);
end;

procedure TfrmMain.shpColorMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  PickColor(Sender);
end;

procedure TfrmMain.WMCopyData(var Msg: TWMCopyData);
begin
  btnChangeColor.Enabled := True;
  shpColor.Brush.Color := TColors.AlphaColorToColor(TColors.HexToAlphaColor(PChar(Msg.CopyDataStruct.lpData)));
end;

end.
