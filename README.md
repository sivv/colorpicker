# Colorpicker #

A color picker app (windows only) made in Delphi Firemonkey that can be called from any application. 

Used on it's own, the color will be copied to the clipboard as a string when selected.

Also, other applications can launch the exe passing parameters.  


    colorpicker.exe [Handle] [DefaultColor]

###Handle

The windows handle that is listening for a WM_COPYDATA message.  ColorPicker will post to this handle the color that was selected.

###DefaultColor

The color that should be pre-selected in the ColorPicker ui.



