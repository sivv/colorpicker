unit cp.utils;

interface

uses
  System.SysUtils, System.UIConsts, System.UITypes, System.Classes, System.Types;

type
  TColors = class
  private
    class function GetMousePoint(var pos : TPoint) : Boolean;
    class function PixelAtMousePos(pos : TPoint; Alpha : Byte = $FF) : TAlphaColor;
  public
    class function CurrentPixel : TAlphaColor;
    class function OSColorToAlphaColor(c : Cardinal; Alpha : Byte = $FF) : TAlphaColor;
    class function ColorToAlphaColor(c : TColor; Alpha : Byte = $FF) : TAlphaColor;
    class function AlphaColorToString(ac : TAlphaColor) : string;
    class function HexToAlphaColor(Color :string) : TAlphaColor;
    class function AlphaColorToColor(ac : TAlphaColor) : TColor;
  end;

implementation

uses
{$IFDEF MSWINDOWS}
  WinAPI.Messages, WinAPI.Windows,
{$ENDIF}
  System.RegularExpressions;

{ TColors }

class function TColors.AlphaColorToColor(ac: TAlphaColor): TColor;
begin
  Result := System.UIConsts.AlphaColorToColor(ac);
end;

class function TColors.AlphaColorToString(ac: TAlphaColor): string;
var
  ar : TAlphaColorRec;
begin
  ar := TAlphaColorRec.Create(ac);
  Result := '#' + IntToHex(ar.A, 2) + IntToHex(ar.R, 2) + IntToHex(ar.G, 2) + IntToHex(ar.B, 2);
end;

class function TColors.OSColorToAlphaColor(c: Cardinal; Alpha : Byte = $FF): TAlphaColor;
begin
{$IFDEF MSWINDOWS}
  TAlphaColorRec(Result).R := GetRValue(c);
  TAlphaColorRec(Result).G := GetGValue(c);
  TAlphaColorRec(Result).B := GetBValue(c);
  TAlphaColorRec(Result).A := Alpha;
{$ENDIF}
end;

class function TColors.ColorToAlphaColor(c: TColor; Alpha : Byte = $FF): TAlphaColor;
begin
  TAlphaColorRec(Result).R := GetRValue(c);
  TAlphaColorRec(Result).G := GetGValue(c);
  TAlphaColorRec(Result).B := GetBValue(c);
  TAlphaColorRec(Result).A := Alpha;
end;

class function TColors.CurrentPixel: TAlphaColor;
var
  pt : TPoint;
begin
  if not GetMousePoint(pt) then
    raise Exception.Create('Cannot locate mouse.');
  Result := PixelAtMousePos(pt);
end;

class function TColors.GetMousePoint(var pos: TPoint): Boolean;
begin
  Result := False;
  {$IFDEF MSWINDOWS}
  Result := GetCursorPos(pos);
  {$ENDIF}
end;

class function TColors.PixelAtMousePos(pos: TPoint; Alpha : Byte = $FF): TAlphaColor;
var
  dc : THandle;
  cr : Cardinal;
begin
  dc := GetWindowDC(GetDesktopWindow);
  cr := getpixel(dc, pos.X, pos.Y);
  Result := TColors.OSColorToAlphaColor(cr, Alpha);
end;

class function TColors.HexToAlphaColor(Color: string): TAlphaColor;
begin
  if not Color.StartsWith('#') then
    Color := '#'+Color;
  if not TRegEx.IsMatch(Color, '^#([0-9a-fA-F]{8})$') then
    Exception.Create('Supplied Color is not a valid hex string.');
  Result := System.UIConsts.StringToAlphaColor(Color);
end;

end.
