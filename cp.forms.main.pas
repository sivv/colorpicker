unit cp.forms.main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Colors, FMX.Layouts, FMX.Controls.Presentation, FMX.Edit, WinAPI.Windows,
  System.Win.Registry, FMX.Effects;

type

  TfrmColorPicker = class(TForm)
    txtColor: TEdit;
    ScrollBox: TScrollBox;
    ColorPanel: TColorPanel;
    ColorBox: TColorBox;
    btnOK: TButton;
    btnCancel: TButton;
    cbClear: TCornerButton;
    ShadowEffect2: TShadowEffect;
    tmrPick: TTimer;
    btnPick: TCornerButton;
    BevelEffect1: TBevelEffect;
    BevelEffect2: TBevelEffect;
    ShadowEffect1: TShadowEffect;
    ShadowEffect3: TShadowEffect;
    ShadowEffect4: TShadowEffect;
    ShadowEffect5: TShadowEffect;
    ShadowEffect6: TShadowEffect;
    procedure ColorPanelChange(Sender: TObject);
    procedure cbClearClick(Sender: TObject);
    procedure txtColorExit(Sender: TObject);
    procedure SelectFromHistory(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tmrPickTimer(Sender: TObject);
    procedure btnPickClick(Sender: TObject);
  private
    FDefaultColor : TAlphaColor;
    FSelectingColors : boolean;
    FSelected: TAlphaColor;
    procedure UpdateTextBox(Color: TAlphaColor);
    procedure SendColorToApp(Color : TAlphaColor);
    procedure RememberColor;
    procedure CopyToClipboard;
    procedure LoadRememberedColors;
  public
    procedure SelectColor(Color : TAlphaColor);
  end;

var
  frmColorPicker: TfrmColorPicker;

implementation

uses System.UIConsts, System.RegularExpressions, FMX.Platform, WinAPI.Messages,
  System.RTTI, cp.forms.overlay, cp.utils;

{$R *.fmx}

function GetScreenScale: Single;
var ScreenService: IFMXScreenService;
begin
  Result := 1;
  if TPlatformServices.Current.SupportsPlatformService (IFMXScreenService, IInterface(ScreenService)) then
    Result := ScreenService.GetScreenScale;
end;

procedure TfrmColorPicker.btnCancelClick(Sender: TObject);
begin
  if not ParamStr(1).IsEmpty then
    SendColorToApp(FDefaultColor);
  Application.Terminate;
end;

procedure TfrmColorPicker.btnOKClick(Sender: TObject);
begin
  if ParamStr(1).IsEmpty then
    CopyToClipboard
  else
    SendColorToApp(FSelected);
  RememberColor;
  Application.Terminate;
end;

procedure TfrmColorPicker.cbClearClick(Sender: TObject);
begin
  SelectColor(StringToAlphaColor('#00000000'));
end;

procedure TfrmColorPicker.ColorPanelChange(Sender: TObject);
begin
  SelectColor(ColorPanel.Color);
end;

procedure TfrmColorPicker.CopyToClipboard;
begin
  (TPlatformServices.Current.GetPlatformService(IFMXClipboardService) as IFMXClipboardService).SetClipboard(TValue.From<String>(txtColor.Text))
end;

procedure TfrmColorPicker.btnPickClick(Sender: TObject);
begin
  tmrPick.Enabled := not tmrPick.Enabled;
  if FSelectingColors then
  begin
    FSelectingColors := False;
    SelectColor(ColorBox.Color);
    TfrmOverlay.HideOverlay;
  end else
  begin
    TfrmOverlay.ShowOverlay(
      procedure
      begin
        btnPickClick(btnPick);
      end
    );
    FSelectingColors := True;
  end;

  if FSelectingColors then
    (TPlatformServices.Current.GetPlatformService(IFMXCursorService) as IFMXCursorService).SetCursor(crCross)
  else
    (TPlatformServices.Current.GetPlatformService(IFMXCursorService) as IFMXCursorService).SetCursor(crArrow);
end;

procedure TfrmColorPicker.FormCreate(Sender: TObject);
begin
  FSelectingColors := False;
  LoadRememberedColors;
  FDefaultColor := StringToAlphaColor('#00000000');
  if not ParamStr(2).IsEmpty then
  begin
    if ParamStr(2).StartsWith('#') and (ParamStr(2).Length = 9) then
    begin
      FDefaultColor := StringToAlphaColor(ParamStr(2));
      exit;
    end;
  end;
  SelectColor(FDefaultColor);
end;

procedure TfrmColorPicker.LoadRememberedColors;
var
  cb : TColorBox;
  sh : TShadowEffect;
  reg : TRegistry;
  i, y : integer;
  bFirst : boolean;
begin
  bFirst := True;
  ScrollBox.BeginUpdate;
  try
    reg := TRegistry.Create;
    try
      reg.RootKey := HKEY_CURRENT_USER;
      reg.OpenKey('\Software\cp\history',True);
      y := 0;
      for i := 0 to 50 do
      begin
        if reg.ValueExists(i.ToString) then
        begin
          cb := TColorBox.Create(Self);
          cb.Parent := ScrollBox;
          cb.Color := reg.ReadInteger(i.ToString);
          if bFirst then
          begin
            SelectColor(cb.Color);
            bFirst := False;
          end;
          cb.Position.X := 7;
          cb.Position.Y := 7+y;
          cb.Width := 40;
          cb.Height := cb.Width;
          cb.OnClick := SelectFromHistory;
          sh := TShadowEffect.Create(cb);
          sh.Parent := cb;
          inc(y, Round(cb.Height)+4);
        end;
      end;
    finally
      reg.Free;
    end;
  finally
    ScrollBox.EndUpdate;
  end;
end;

procedure TfrmColorPicker.RememberColor;
var
  reg : TRegistry;
  i: Integer;
  y: Integer;
begin
  reg := TRegistry.Create;
  try
    reg.RootKey := HKEY_CURRENT_USER;
    reg.OpenKey('\Software\cp\history', True);
    for i := 0 to 50 do
    begin
      if reg.ValueExists(i.ToString) then
      begin
        if Cardinal(reg.ReadInteger(i.ToString)) = FSelected then
        begin
          for y := i downto 1 do
          begin
            if reg.ValueExists(y.ToString) then
              reg.WriteInteger((y).ToString, reg.ReadInteger((y-1).ToString));
          end;
          reg.WriteInteger('0', Integer(FSelected));
          exit;
        end;
      end;
    end;
    for y := 49 downto 0 do
    begin
      if reg.ValueExists((y).ToString) then
        reg.WriteInteger((y+1).ToString, reg.ReadInteger((y).ToString));
    end;
    reg.WriteInteger('0', Integer(FSelected));
  finally
    reg.Free;
  end;
end;

procedure TfrmColorPicker.SelectColor(Color: TAlphaColor);
begin
  UpdateTextBox(Color);
  ColorBox.Color := Color;
  ColorPanel.Color := Color;
  if not FSelectingColors then
    FSelected := Color;
end;

procedure TfrmColorPicker.SelectFromHistory(Sender: TObject);
begin
  SelectColor(TColorBox(Sender).Color);
end;

procedure TfrmColorPicker.SendColorToApp(Color: TAlphaColor);
var
  DataStruct : CopyDataStruct;
  h : HWND;
  sColor : string;
begin
  sColor := TColors.AlphaColorToString(Color);
  DataStruct.dwData := 0;
  DataStruct.cbData := sColor.Length * SizeOf(Char);
  DataStruct.lpData := PChar(sColor);
  h := HWND(StrToIntDef(ParamStr(1),0));
  BringWindowToTop(h);
  SetForegroundWindow(h);
  SendMessage(h, WM_COPYDATA, NativeUInt(Handle), NativeInt(@DataStruct));
end;

procedure TfrmColorPicker.tmrPickTimer(Sender: TObject);
begin
  if FSelectingColors then
  begin
    SelectColor(TColors.CurrentPixel);
  end;
end;

procedure TfrmColorPicker.UpdateTextBox(Color: TAlphaColor);
begin
  txtColor.Text := TColors.AlphaColorToString(Color);
end;

procedure TfrmColorPicker.txtColorExit(Sender: TObject);
begin
  if not txtColor.Text.StartsWith('#') then
    txtColor.Text := '#'+txtColor.Text;
  try
    SelectColor(TColors.HexToAlphaColor(txtColor.Text))
  except
    UpdateTextBox(FSelected);
  end;
end;

end.
